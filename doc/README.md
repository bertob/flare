# Development Documentation

This folder contains information regarding the development of Flare.
This includes information about [compilation](https://gitlab.com/schmiddi-on-mobile/flare/-/tree/master/doc/compilation.md?ref_type=heads) or the [architecture](https://gitlab.com/schmiddi-on-mobile/flare/-/tree/master/doc/architecture.md?ref_type=heads) defining the general structure of the project.
It also includes [links to additional documentation](https://gitlab.com/schmiddi-on-mobile/flare/-/tree/master/doc/additional-documentation.md?ref_type=heads) like for example presage or GTK.
You can also find a short [MR checklist](https://gitlab.com/schmiddi-on-mobile/flare/-/tree/master/doc/mr-checklist.md?ref_type=heads) and a documentation of the [commit style](https://gitlab.com/schmiddi-on-mobile/flare/-/tree/master/doc/commit-style.md?ref_type=heads).

As always, feel free to ask questions or request some help if you get stuck somewhere, e.g. via [Matrix](https://matrix.to/#/%23flare-signal:matrix.org).

